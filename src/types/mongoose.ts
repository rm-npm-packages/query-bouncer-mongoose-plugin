

// eslint-disable-next-line import/no-unresolved
import { Request } from 'express';
import mongodb = require('mongodb');


interface MongoBouncerOptions {
  Request?: Request;
  Disabled?: boolean;
}

declare module 'mongoose' {

  export interface SaveOptions {
    MongoBouncer?: MongoBouncerOptions;
  }

  export interface ModelOptions {
    MongoBouncer?: MongoBouncerOptions;
  }

  /* eslint-disable @typescript-eslint/ban-types */
  /* eslint-disable @typescript-eslint/no-unused-vars */
  export interface Query<ResultType, DocType, THelpers = {}, RawDocType = DocType>{
    _collection: Collection
  }

  export interface InternalCache{
    saveOptions: ModelOptions,
    _id: mongodb.ObjectId,

  }
  export interface Document {
    $__: InternalCache
  }

  export interface QueryOptions {
    MongoBouncer?: MongoBouncerOptions;
  }
  
}