export default interface BlogPost {
  Title: string
  Description: string
  Category: string
}