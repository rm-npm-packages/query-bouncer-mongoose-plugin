import { QbConfig } from '../../..';

const defaultConfig = new QbConfig({
  baseUrl: 'http://my-host.com',
});

export default defaultConfig;