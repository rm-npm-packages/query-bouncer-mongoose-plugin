import { MockAdapter } from '../index';
import sinon from 'sinon';
import referee from '@sinonjs/referee';
import QbConfig from '../src/classes/class.QbConfig';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose, { Mongoose, Model, Schema } from 'mongoose';
import { expect } from 'chai';
import MongoBouncer from '../index';
import { Request } from 'express';

const assert = referee.assert;

async function connect(connectionString:string, mongoose:Mongoose): Promise<void> {
  return new Promise((res) => {
    console.log('Connecting to ' + connectionString);  
    mongoose.connect(connectionString, {
    }, () => {
      res();
    });
  });
}

describe('replace Axios', () => {

  let mongodb: MongoMemoryServer;
  let mymodel: Model<any>;
  let config: QbConfig;

  before(async () => {
    mongodb = new MongoMemoryServer();
    const uri = await mongodb.getUri();
    await connect(uri, mongoose);
    
    const schema = new Schema({
      Value: String,
      Category: String,
    });
    config = new QbConfig({});
    schema.plugin(MongoBouncer, config);
    mymodel = mongoose.model('test', schema );
  });

  beforeEach(async () => {
    mymodel.create([
      { Value: 'Should be returned', Category: 'cat1' },
      { Value: 'Should not be returned', Category: 'cat2' },

    ]);
  });

  afterEach( async () => {
    await mongoose.connection.dropDatabase();
  });

  after(() => {
    mongoose.connection.close();
  });

  it('the mock adapter will mock axios requests', async () => {
    // Prepare
    const qBouncer = new MockAdapter(config);
    const spy = sinon.spy(config.axios, 'put');
    qBouncer.mock({
      collection: mymodel.collection.collectionName,
      right: 'read',
      response: {
        query: { $or: [{ Category: 'cat1' }] }
      }
    });

    // Execute
    const results = await mymodel.find({},null, {
      MongoBouncer: {
        Request: {} as Request
      }
    });

    // Assert
    expect(results.length).to.equal(1);
    assert(spy.calledOnce);
  });
});